# tape-ng

tape-ng is an attempt to write a program for compiling compact cassette
mixtapes by a new generation of mixtape creators that wants to use their
computer rather than vinyl or CD players to get the music onto tape.

Current status: GUI draft created, code needs to be written.
